import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreComponent } from './core.component';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../auth/guard/auth.guard';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  { path: '', component: CoreComponent, canActivate: [AuthGuard] }
];

@NgModule({
  declarations: [CoreComponent],
  imports: [
    BrowserModule,
    FormsModule,
    CommonModule,
    RouterModule.forRoot(routes)
  ]
})
export class CoreModule { }
