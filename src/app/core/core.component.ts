import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-core',
  templateUrl: './core.component.html',
  styleUrls: ['./core.component.css']
})
export class CoreComponent implements OnInit {

  name = 'Angular 6';
  safeSrc: SafeResourceUrl;
  constructor(private sanitizer: DomSanitizer) {
    // tslint:disable-next-line:max-line-length
    this.safeSrc =  this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/c9F5kMUfFKk');
  }
  // &lc=UgzHzEyKJE7ZXVOLvwF4AaABAg

  ngOnInit() {
  }

}
