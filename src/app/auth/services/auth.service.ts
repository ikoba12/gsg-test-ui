import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly userId = 'userId';

  constructor(
    private http: HttpClient
  ) {
  }

  login(data: any) {
    const paramData = new HttpParams()
      .set('username', data.username)
      .set('password', data.password);

    return this.http.post(`${environment.apiUrl}/user/login`, paramData);
  }

  register(data: any) {
    const paramData = {
      username: data.username,
      password: data.password,
      jobInterval: data.jobInterval,
      country: data.country
    };


    return this.http.post(`${environment.apiUrl}/user/register`, paramData);
  }

  setUserId(userId) {
    localStorage.setItem(this.userId, userId);
  }

  getUserId() {
    return localStorage.getItem(this.userId);
  }

}
