import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  canActivate() {
  //   if ( !this.authservice.isLoggedIn() ) {
  //     this.route.navigate(['/login']);
  //   }
    return true;
  }
}
