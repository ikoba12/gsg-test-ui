import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from './services/auth.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit, OnDestroy {

  $destroy: Subject<boolean> = new Subject<boolean>();
  loginForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authServ: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  submit() {
    console.log(this.loginForm.value);
    if (!this.loginForm.valid) {
      return;
    }
    this.router.navigate(['']);
    this.authServ.login(this.loginForm.value).pipe(takeUntil(this.$destroy)).subscribe(data => {
      console.log(data);
      // this.router.navigate(['']);
    });
  }

  ngOnDestroy(): void {
    this.$destroy.next(true);
    this.$destroy.unsubscribe();
  }

  navigateToRegister() {
    this.router.navigate(['register']);
  }
}
