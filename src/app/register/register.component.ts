import { Component, OnInit } from '@angular/core';
import {Subject} from "rxjs";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../auth/services/auth.service";
import {Router} from "@angular/router";
import {takeUntil} from "rxjs/operators";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {


  $destroy: Subject<boolean> = new Subject<boolean>();
  registerForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authServ: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.registerForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      jobInterval: ['', [Validators.required, Validators.max(60), Validators.min(1)]],
      country: ['', Validators.required]
    });
  }

  submit() {
    console.log(this.registerForm.value);
    if (!this.registerForm.valid) {
      return;
    }
    this.authServ.register(this.registerForm.value).pipe(takeUntil(this.$destroy)).subscribe(data => {
      console.log(data);
      // this.router.navigate(['']);
    });
  }

  back() {
    this.router.navigate(['login']);
  }
  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy(): void {
    this.$destroy.next(true);
    this.$destroy.unsubscribe();
  }

}
